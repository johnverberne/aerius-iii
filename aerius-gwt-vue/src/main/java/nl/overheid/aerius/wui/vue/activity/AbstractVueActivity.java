/*
 * Copyright Dutch Ministry of Agriculture, Nature and Food Quality
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.vue.activity;

import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.vue.VueComponentFactory;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.activity.Activity;
import nl.overheid.aerius.wui.vue.AcceptsOneComponent;
import nl.overheid.aerius.wui.widget.HasEventBus;

public abstract class AbstractVueActivity<P, V extends IsVueComponent, F extends VueComponentFactory<V>>
    implements Activity<P, AcceptsOneComponent>, HasEventBus {
  private final F factory;
  private EventBus eventBus;

  public AbstractVueActivity(final F factory) {
    this.factory = factory;
  }

  @Override
  public void onStop() {}

  @Override
  public String mayStop() {
    return null;
  }

  @Override
  public void onStart(final AcceptsOneComponent panel) {
    if (panel instanceof HasEventBus) {
      ((HasEventBus) panel).setEventBus(eventBus);
    }
    panel.setComponent(factory, getPresenter());
  }

  @Override
  public abstract P getPresenter();

  public void setView(final V view) {}

  @Override
  public void setEventBus(final EventBus eventBus) {
    this.eventBus = eventBus;
  }
}
