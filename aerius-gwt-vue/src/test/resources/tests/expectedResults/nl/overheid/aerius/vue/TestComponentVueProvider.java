package nl.overheid.aerius.vue;

import com.google.inject.Inject;
import com.google.inject.Provider;
import java.lang.Override;

public final class TestComponentVueProvider implements Provider<TestComponent> {
  private final TestComponentProvider factory;

  @Inject
  public TestComponentVueProvider(final TestComponentProvider factory) {
    this.factory = factory;
  }

  @Override
  public void get() {
    return factory.create();
  }
}
