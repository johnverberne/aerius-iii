# AERIUS III

## Prerequisites

To install and configure this project, the following tools are required:

- `mvn` (Maven)
- `git`
- `javac` Java JDK 8+

Optional:

- `tmux`
- `entr`

### Arch Linux

```
sudo pacman -Syu mvn git jdk8-openjdk
```

### Ubuntu / Debian

```
sudo apt install git mvn openjdk-8-jdk
```

## Cloning this project

Clone the project using:

```
git clone git@gitlab.com:yogh/aerius-iii.git
```

## Configuration

### Quick start

To quickly configure the git repository and development mode, simply run:

```
./init.sh
```

And to start all development servers, run:

```
./serve.sh
```

This last script requires the optional dependencies.

An explanation of what this does is described in the following few paragraphs. Otherwise skip to [Running development mode](#running-development-mode).

### Configure git repository

We're depending on a fork of vue-gwt that contains various changes which have not yet been released to maven central. To initialize and update these submodules, run the following commands:

```
git submodule init
git submodule update --remote
```

### Configure development mode

Install the projects maintained through submodules. The `dependencies` profile includes a subset of projects which need to be compiled and added to the maven cache, so that they won't later be downloaded from maven central.

```
mvn install -Pdependencies
```

## Running development mode

Next, to run a GWT development environment we need to run two types of servers:

1. A web server to serve static html/js files, fonts, assets, etc.
2. A GWT codeserver to recompile Java files fetched by the static recompile-requester.

This is a multi-module project reconfigured to follow TBroyer's gwt maven setup. Thanks to this setup it is now much easier to run these two servers using just Maven, and avoid dealing with (most of) the Eclipse or IntelliJ configuration hell.

### Instructions

There's two convenience scripts in the project's root dir which will run the servers.

From the root directory, in one terminal window, run:

```
./codeserver.sh
```

and in another terminal window, run:

```
./webserver.sh
```

If all goes well, both servers will be up and running, and the application will be available in the browser on:

```
http://localhost:8080
```

#### Informational

The codeserver script consists simply of a maven goal for the `gwt` plugin:

```
mvn gwt:codeserver -am
```

The webserver script consists of a maven goal for the `jetty` plugin, and an argument to run it in the dev profile:

```
mvn jetty:run -pl aerius-iii-server -am -Denv=dev
```

### Optional: Run a livereload checker

Angular and other web frameworks often boast a livereload feature which for GWT has been sorely lacking. Not many tools are available to make this happen, so a light and rudimentary way of doing it has been scrapped together. It uses a tool called `entr` to monitor file changes.

For more information on this tool, see: http://eradman.com/entrproject/ It is available for most package managers.

While in dev mode, an additional script has been added to the index which will poll a specific file on the server, if this file changes, it will issue a web page reload. The file changes when `entr` notices source files have changed.

To run it, simply run:

```
./livereload.sh
```

Currently, this tool indirectly depends on an IDE and its annotation processor for changes in HTML and SCSS to be reflected, this might possibly be remidied in the future.

#### Hint

> Unfortunately, this produces a large number of requests in the dev tools' network tab. To ignore this (for Chrome dev tools), add `-reload` to the network filter.

### Optional: Run all of the above through tmux

Alternatively, you can run all of the above in a tmux session.

You need to additionally install `tmux` for this to work.

Run:

```
./serve.sh
```

This `tmux` session will contain three panels, one for the `codeserver`, one for the `webserver`, and one for the `livereload checker`.

## Configure the Eclipse IDE

1. Import the project root into eclipse as a maven project.

2. Install the m2e-apt plugin, so Eclipse will process annotations.

See https://marketplace.eclipse.org/content/m2e-apt

When installed, enable annotation processing by setting the

```
Window > Preferences > Maven > Annotation Processing > Annotation Processing Mode
```

setting, to

```
Automatically configure JDT APT
```

For further information, please refer to:

https://vuegwt.github.io/vue-gwt/guide/project-setup.html#annotation-processing

3. Install the gwt-vue plugin, to make eclipse run the annotation processor when editing .html files

See https://github.com/VueGWT/vue-gwt-eclipse-plugin

For further information, please refer to:

https://vuegwt.github.io/vue-gwt/guide/project-setup.html#vue-gwt-eclipse-plugin

