package nl.overheid.aerius.wui.application.components.nav;

import java.util.ArrayList;
import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasActivated;
import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import jsinterop.annotations.JsProperty;
import nl.overheid.aerius.wui.application.components.logo.AeriusLogoComponent;
import nl.overheid.aerius.wui.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.place.Place;
import nl.overheid.aerius.wui.place.PlaceController;

@Component(name = "aer-nav",
    components = {
        AeriusLogoComponent.class,
        NavigationItemComponent.class
    })
public class NavigationComponent implements IsVueComponent, HasActivated {
  interface NavigationComponentEventBinder extends EventBinder<NavigationComponent> {}

  private final NavigationComponentEventBinder EVENT_BINDER = GWT.create(NavigationComponentEventBinder.class);

  @Prop EventBus eventBus;

  @Prop @JsProperty List<NavigationItem> config;

  @Data Place place;

  @Inject PlaceController placeController;

  @Computed
  public List<NavigationItem> getConfiguration() {
    return config == null ? new ArrayList<>() : config;
  }

  @EventHandler
  public void onPlaceChangeEvent(final PlaceChangeEvent e) {
    place = e.getValue();
  }

  @Override
  public void activated() {
    place = placeController.getPlace();
    GWT.log("Nav component activated: " + place);

    if (eventBus != null) {
      EVENT_BINDER.bindEventHandlers(this, eventBus);
    }
  }
}
