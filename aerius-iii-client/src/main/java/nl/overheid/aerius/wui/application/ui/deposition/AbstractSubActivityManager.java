package nl.overheid.aerius.wui.application.ui.deposition;

import java.util.function.Consumer;

import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.place.Place;
import nl.overheid.aerius.wui.widget.HasEventBus;

public abstract class AbstractSubActivityManager<V, P, S> {
  private EventBus eventBus;

  private Consumer<Place> redirector;

  private V view;
  private P place;

  private boolean delegate;

  public boolean delegate(final EventBus eventBus, final P place, final Consumer<Place> redirector) {
    this.eventBus = eventBus;
    this.place = place;
    this.redirector = redirector;

    return tryCanDelegate();
  }

  public void setView(final V view) {
    this.view = view;

    doDelegate();
  }

  private void doDelegate() {
    if (!delegate) {
      return;
    }

    delegate = false;
    final S act = getActivity(place, view);
    if (act == null) {
      return;
    }

    if (act instanceof HasEventBus) {
      ((HasEventBus) act).setEventBus(eventBus);
    }
  }

  private S redirect(final Place place) {
    redirector.accept(place);
    return null;
  }

  private boolean tryCanDelegate() {
    final Place redirect = getRedirect(place);

    if (redirect == null) {
      scheduleDelegate();
    } else {
      redirect(redirect);
    }

    return redirect == null;
  }

  private void scheduleDelegate() {
    delegate = true;
    if (view != null) {
      doDelegate();
    }
  }

  protected abstract Place getRedirect(P place);

  protected abstract S getActivity(P place, V view);
}
