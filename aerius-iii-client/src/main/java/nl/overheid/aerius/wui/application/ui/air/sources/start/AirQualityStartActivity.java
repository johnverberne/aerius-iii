package nl.overheid.aerius.wui.application.ui.air.sources.start;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.wui.application.components.dummy.SimpleDummyComponentFactory;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceCreatePlace;
import nl.overheid.aerius.wui.application.ui.air.AirQualitySubActivity;
import nl.overheid.aerius.wui.application.ui.air.AirQualityView;
import nl.overheid.aerius.wui.place.PlaceController;

public class AirQualityStartActivity implements AirQualitySubActivity {
  @Inject PlaceController placeController;

  @Inject
  public AirQualityStartActivity(@Assisted final AirQualityView view) {
    view.setComponent(AirQualityStartViewFactory.get(), this);
    view.setMap(SimpleDummyComponentFactory.get());
  }

  public void goToCreate() {
    placeController.goTo(new SourceCreatePlace());
  }
}
