package nl.overheid.aerius.wui.application.place.deposition;

public class DepositionSourceCreatePlace extends DepositionSourcePlace {
  public DepositionSourceCreatePlace() {
    super(DepositionTokenizers.SOURCE_CREATE);
  }
}
