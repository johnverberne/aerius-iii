package nl.overheid.aerius.wui.application.ui.air;

import nl.overheid.aerius.wui.application.ui.main.MainPresenter;

public interface AirQualityPresenter extends MainPresenter {
  void setView(AirQualityView airQualityView);
}
