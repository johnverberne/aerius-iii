package nl.overheid.aerius.wui.application.ui.air.sources.create;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.wui.application.components.dummy.SimpleDummyComponentFactory;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceListPlace;
import nl.overheid.aerius.wui.application.ui.air.AirQualitySubActivity;
import nl.overheid.aerius.wui.application.ui.air.AirQualityView;
import nl.overheid.aerius.wui.place.PlaceController;

public class AirQualityCreateActivity implements AirQualitySubActivity {
  @Inject PlaceController placeController;

  @Inject
  public AirQualityCreateActivity(@Assisted final AirQualityView view) {
    view.setComponent(AirQualityCreateViewFactory.get(), this);
    view.setMap(SimpleDummyComponentFactory.get());
  }

  public void goToList() {
    placeController.goTo(new SourceListPlace());
  }
}
