package nl.overheid.aerius.wui.application.util;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

public enum ThemeOption {
  AIR_QUALITY("air-quality"),
  DEPOSITION,
  HEALTH,
  ODOR,
  SAFETY,
  SOIL,
  SOUND,
  WATER;

  private String key;

  private ThemeOption() {
    this.key = name().toLowerCase();
  }

  private ThemeOption(final String key) {
    this.key = key;
  }

  public String getKey() {
    return key;
  }

  public static Optional<ThemeOption> fromKey(final String key) {
    return Stream.of(values())
        .filter(v -> Objects.equals(v.getKey(), key))
        .findFirst();
  }
}
