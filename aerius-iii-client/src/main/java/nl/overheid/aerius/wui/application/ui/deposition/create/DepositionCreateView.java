package nl.overheid.aerius.wui.application.ui.deposition.create;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;

import nl.overheid.aerius.wui.application.components.dummy.SimpleSecondaryDummyComponent;
import nl.overheid.aerius.wui.application.components.views.DefaultTitleView;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    DefaultTitleView.class,
    SimpleSecondaryDummyComponent.class })
public class DepositionCreateView extends BasicVueView {
  @Prop DepositionCreateActivity presenter;
}
