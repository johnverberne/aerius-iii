/*
 * Copyright Dutch Ministry of Agriculture, Nature and Food Quality
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.dev;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.wui.command.PlaceChangeCommand;
import nl.overheid.aerius.wui.dev.DevelopmentObserver;
import nl.overheid.aerius.wui.dev.GWTProd;
import nl.overheid.aerius.wui.event.PlaceChangeEvent;

@Singleton
public class ApplicationDevelopmentObserver implements DevelopmentObserver {
  interface DevelopmentObserverEventBinder extends EventBinder<ApplicationDevelopmentObserver> {}

  private final DevelopmentObserverEventBinder EVENT_BINDER = GWT.create(DevelopmentObserverEventBinder.class);

  @Inject
  public ApplicationDevelopmentObserver(final EventBus eventBus) {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  // @SuppressWarnings("rawtypes")
  // @EventHandler(handles = {})
  // public void onSimpleGenericEvent(final SimpleGenericEvent e) {
  // log(e.getClass().getSimpleName(), e.getValue());
  // }
  //
  // @SuppressWarnings("rawtypes")
  // @EventHandler(handles = {})
  // public void onSimpleGenericCommand(final SimpleGenericCommand c) {
  // log(c.getClass().getSimpleName(), c.getValue());
  // }

  @EventHandler
  public void onPlaceChangeCommand(final PlaceChangeCommand e) {
    log("PlaceChangeCommand", e.getValue());
  }

  @EventHandler
  public void onPlaceChangeEvent(final PlaceChangeEvent e) {
    log("PlaceChangeEvent", e.getValue());

    Scheduler.get().scheduleDeferred(() -> brbr());
  }

  private void brbr() {
    logRaw("");
  }

  private void log(final String origin) {
    logRaw("[" + origin + "]");
  }

  private void log(final String origin, final Object val) {
    logRaw("[" + origin + "] " + String.valueOf(val));
  }

  private void logRaw(final String string) {
    GWTProd.log(string);
  }
}
