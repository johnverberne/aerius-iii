package nl.overheid.aerius.wui.application.ui.deposition;

import nl.overheid.aerius.wui.application.place.deposition.DepositionSourceCreatePlace;
import nl.overheid.aerius.wui.application.place.deposition.DepositionSourceImportPlace;
import nl.overheid.aerius.wui.application.place.deposition.DepositionSourceListPlace;
import nl.overheid.aerius.wui.application.ui.deposition.create.DepositionCreateActivity;
import nl.overheid.aerius.wui.application.ui.deposition.list.DepositionListActivity;
import nl.overheid.aerius.wui.application.ui.deposition.start.DepositionStartActivity;

public interface DepositionActivityFactory {
  DepositionStartActivity createDepositionStartActivity(DepositionView view);

  DepositionStartActivity createDepositionStartActivity(DepositionView view, DepositionSourceImportPlace place);

  DepositionCreateActivity createDepositionCreateActivity(DepositionView view, DepositionSourceCreatePlace place);

  DepositionListActivity createDepositionListActivity(DepositionView view, DepositionSourceListPlace place);
}
