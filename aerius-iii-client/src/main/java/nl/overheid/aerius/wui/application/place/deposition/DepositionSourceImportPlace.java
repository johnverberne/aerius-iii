package nl.overheid.aerius.wui.application.place.deposition;

public class DepositionSourceImportPlace extends DepositionSourcePlace {
  public DepositionSourceImportPlace() {
    super(DepositionTokenizers.SOURCE_IMPORT);
  }
}
