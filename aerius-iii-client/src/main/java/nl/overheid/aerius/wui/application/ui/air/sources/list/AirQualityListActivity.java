package nl.overheid.aerius.wui.application.ui.air.sources.list;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.wui.application.components.dummy.SimpleSecondaryDummyComponentFactory;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceImportPlace;
import nl.overheid.aerius.wui.application.ui.air.AirQualitySubActivity;
import nl.overheid.aerius.wui.application.ui.air.AirQualityView;
import nl.overheid.aerius.wui.place.PlaceController;

public class AirQualityListActivity implements AirQualitySubActivity {
  @Inject PlaceController placeController;

  @Inject
  public AirQualityListActivity(@Assisted final AirQualityView view) {
    view.setComponent(AirQualityListViewFactory.get(), this);

    view.setMap(SimpleSecondaryDummyComponentFactory.get());
  }

  public void goToImport() {
    placeController.goTo(new SourceImportPlace());
  }
}
