package nl.overheid.aerius.wui.application.place.deposition;

import nl.overheid.aerius.wui.place.PlaceTokenizer;

public class DepositionTokenizers {
  private static final String SOURCES = "source";

  public static final PlaceTokenizer<DepositionSourcePlace> SOURCE = new DepositionPlace.Tokenizer<>(
      () -> new DepositionSourcePlace(),
      SOURCES);

  public static final PlaceTokenizer<DepositionSourceImportPlace> SOURCE_IMPORT = new DepositionPlace.Tokenizer<>(
      () -> new DepositionSourceImportPlace(),
      SOURCES, "start");
  public static final PlaceTokenizer<DepositionSourceCreatePlace> SOURCE_CREATE = new DepositionPlace.Tokenizer<>(
      () -> new DepositionSourceCreatePlace(),
      SOURCES, "create");
  public static final PlaceTokenizer<DepositionSourceListPlace> SOURCE_LIST = new DepositionPlace.Tokenizer<>(
      () -> new DepositionSourceListPlace(),
      SOURCES, "list");
}
