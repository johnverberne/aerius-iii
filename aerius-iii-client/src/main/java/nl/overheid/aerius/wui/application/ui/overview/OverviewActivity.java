package nl.overheid.aerius.wui.application.ui.overview;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.wui.application.place.OverviewPlace;
import nl.overheid.aerius.wui.application.place.ThemePlaceProvider;
import nl.overheid.aerius.wui.application.util.Theme;
import nl.overheid.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.vue.activity.AbstractVueActivity;

public class OverviewActivity extends AbstractVueActivity<OverviewActivity, OverviewView, OverviewViewFactory> {
  private final ThemePlaceProvider placeProvider;

  @Inject PlaceController placeController;

  @Inject
  public OverviewActivity(final @Assisted OverviewPlace overviewPlace, final ThemePlaceProvider placeProvider) {
    super(OverviewViewFactory.get());

    this.placeProvider = placeProvider;
  }

  @Override
  public OverviewActivity getPresenter() {
    return this;
  }

  public void selectTheme(final Theme theme) {
    if (!theme.isEnabled()) {
      return;
    }

    placeController.goTo(placeProvider.getPlace(theme.getOption()));
  }
}
