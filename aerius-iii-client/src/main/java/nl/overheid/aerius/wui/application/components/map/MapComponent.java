package nl.overheid.aerius.wui.application.components.map;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.google.inject.Inject;

import jsinterop.annotations.JsMethod;
import nl.overheid.aerius.wui.application.components.dummy.SimpleDummyComponent;
import nl.overheid.aerius.wui.application.place.OverviewPlace;
import nl.overheid.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.vue.BasicVueComponent;

@Component(name = "aer-map",
    components = { SimpleDummyComponent.class },
    useFactory = false)
public class MapComponent extends BasicVueComponent {
  @Inject PlaceController placeController;

  @JsMethod
  public void onLogoClick() {
    placeController.goTo(new OverviewPlace());
  }
}
