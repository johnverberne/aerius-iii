package nl.overheid.aerius.wui.application.ui.air;

import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceImportPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceListPlace;
import nl.overheid.aerius.wui.application.ui.air.sources.create.AirQualityCreateActivity;
import nl.overheid.aerius.wui.application.ui.air.sources.list.AirQualityListActivity;
import nl.overheid.aerius.wui.application.ui.air.sources.start.AirQualityStartActivity;

public interface AirQualityActivityFactory {
  AirQualityStartActivity createAirQualityStartActivity(AirQualityView view, SourceImportPlace place);

  AirQualityCreateActivity createAirQualityCreateActivity(AirQualityView view, SourceCreatePlace place);

  AirQualityListActivity createAirQualityListActivity(AirQualityView view, SourceListPlace place);
}
