package nl.overheid.aerius.wui.application.ui.main;

import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsProperty;
import nl.overheid.aerius.wui.application.components.map.MapComponent;
import nl.overheid.aerius.wui.application.components.nav.NavigationComponent;
import nl.overheid.aerius.wui.application.components.nav.NavigationItem;

@Component(name = "aer-main",
    components = {
        NavigationComponent.class,
        MapComponent.class
    })
public class MainView implements IsVueComponent {
  @Prop EventBus eventBus;
  @Prop @JsProperty List<NavigationItem> navConfig;
}
