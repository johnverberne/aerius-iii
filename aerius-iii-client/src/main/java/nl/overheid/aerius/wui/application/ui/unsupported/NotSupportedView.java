package nl.overheid.aerius.wui.application.ui.unsupported;

import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;

import jsinterop.annotations.JsProperty;
import nl.overheid.aerius.wui.application.util.Theme;
import nl.overheid.aerius.wui.application.util.UglyBoilerPlate;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component
public class NotSupportedView extends BasicVueView {
  @Data @JsProperty List<Theme> themes = UglyBoilerPlate.getApplicationThemes();
}
