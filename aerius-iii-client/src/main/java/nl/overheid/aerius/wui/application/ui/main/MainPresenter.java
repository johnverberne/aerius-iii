package nl.overheid.aerius.wui.application.ui.main;

public interface MainPresenter {
  void goToOverview();
}
