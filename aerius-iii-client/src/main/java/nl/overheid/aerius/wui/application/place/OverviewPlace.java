package nl.overheid.aerius.wui.application.place;

import nl.overheid.aerius.wui.place.ApplicationPlace;

public class OverviewPlace extends ApplicationPlace {
  public OverviewPlace() {
    super(ApplicationTokenizers.OVERVIEW);
  }
}
