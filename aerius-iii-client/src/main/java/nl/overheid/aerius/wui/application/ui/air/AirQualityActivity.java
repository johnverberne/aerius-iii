package nl.overheid.aerius.wui.application.ui.air;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.activity.DelegableActivity;
import nl.overheid.aerius.wui.application.components.nav.NavigationConfiguration;
import nl.overheid.aerius.wui.application.components.nav.NavigationItem;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.OverviewPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.AirQualityPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.LayerPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.MeasurePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.PreferencePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.ResultPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourcePlace;
import nl.overheid.aerius.wui.application.resources.R;
import nl.overheid.aerius.wui.place.Place;
import nl.overheid.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.vue.activity.AbstractVueActivity;

public class AirQualityActivity extends AbstractVueActivity<AirQualityPresenter, AirQualityView, AirQualityViewFactory>
    implements AirQualityPresenter, DelegableActivity<AirQualityPlace> {

  @Inject PlaceController placeController;

  private final AirQualityActivityManager delegator;

  @Inject
  public AirQualityActivity(final AirQualityActivityManager delegator) {
    super(AirQualityViewFactory.get());
    this.delegator = delegator;
  }

  @Override
  public AirQualityPresenter getPresenter() {
    return this;
  }

  @Override
  public void goToOverview() {
    placeController.goTo(new OverviewPlace());
  }

  @Override
  public boolean delegate(final EventBus eventBus, final AirQualityPlace place) {
    GWT.log("Delegating for place: " + place);

    final boolean delegated = delegator.delegate(eventBus, place, p -> placeController.goTo(p));

    GWT.log("Delegated: " + delegated);

    return delegated;
  }

  @Override
  public void setView(final AirQualityView view) {
    delegator.setView(view);

    final NavigationConfiguration nav = new NavigationConfiguration();

    nav.add(NavigationItem.createPlaceItem(M.messages().themeAirQualitySources(), R.images().themeAirQualitySources(),
        p -> p instanceof SourcePlace,
        placeController, new SourcePlace()));
    nav.add(NavigationItem.createPlaceItem(M.messages().themeAirQualityCalculationPoints(), R.images().themeAirQualitySources(),
        p -> p instanceof CalculationPointPlace,
        placeController, new CalculationPointPlace()));
    nav.add(NavigationItem.createPlaceItem(M.messages().themeAirQualityMeasures(), R.images().themeAirQualitySources(),
        p -> p instanceof MeasurePlace,
        placeController, new MeasurePlace()));
    nav.add(NavigationItem.createPlaceItem(M.messages().themeAirQualityPreferences(), R.images().themeAirQualitySources(),
        p -> p instanceof PreferencePlace,
        placeController, new PreferencePlace()));
    nav.add(NavigationItem.createPlaceItem(M.messages().themeAirQualityResults(), R.images().themeAirQualitySources(),
        p -> p instanceof ResultPlace,
        placeController, new ResultPlace()));
    nav.add(NavigationItem.createPlaceItem(M.messages().themeAirQualityLayers(), R.images().themeAirQualitySources(),
        p -> p instanceof LayerPlace,
        placeController, new LayerPlace()));

    view.setNavigation(nav);
  }

  @Override
  public boolean isDelegable(final Place place) {
    return place instanceof AirQualityPlace;
  }
}
