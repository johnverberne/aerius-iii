package nl.overheid.aerius.wui.application.components.views;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

@Component
public class DefaultTitleView implements IsVueComponent {
  @Prop String title;
}
