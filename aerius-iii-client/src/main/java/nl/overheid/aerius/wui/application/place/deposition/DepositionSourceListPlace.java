package nl.overheid.aerius.wui.application.place.deposition;

public class DepositionSourceListPlace extends DepositionSourcePlace {
  public DepositionSourceListPlace() {
    super(DepositionTokenizers.SOURCE_LIST);
  }
}
