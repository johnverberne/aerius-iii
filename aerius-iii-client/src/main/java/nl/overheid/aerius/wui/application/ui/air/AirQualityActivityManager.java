package nl.overheid.aerius.wui.application.ui.air;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;

import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.AirQualityPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceImportPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceListPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourcePlace;
import nl.overheid.aerius.wui.application.ui.deposition.AbstractSubActivityManager;
import nl.overheid.aerius.wui.place.Place;

public class AirQualityActivityManager extends AbstractSubActivityManager<AirQualityView, AirQualityPlace, AirQualitySubActivity> {
  private final AirQualityActivityFactory activityFactory;

  @Inject
  public AirQualityActivityManager(final AirQualityActivityFactory activityFactory) {
    this.activityFactory = activityFactory;
  }

  @Override
  public AirQualitySubActivity getActivity(final AirQualityPlace place, final AirQualityView view) {
    if (place instanceof SourceImportPlace) {
      return activityFactory.createAirQualityStartActivity(view, ((SourceImportPlace) place));
    } else if (place instanceof SourceCreatePlace) {
      return activityFactory.createAirQualityCreateActivity(view, (SourceCreatePlace) place);
    } else if (place instanceof SourceListPlace) {
      return activityFactory.createAirQualityListActivity(view, (SourceListPlace) place);
    } else {
      throw new RuntimeException("Could not create sub-activity for AirQualityActivity");
    }
  }

  @Override
  protected Place getRedirect(final AirQualityPlace place) {
    GWT.log("Getting redirect for: " + place);

    Place redirect = null;
    if (place.getClass().equals(SourcePlace.class)) {
      redirect = new SourceImportPlace();
    } else if (place.getClass().equals(CalculationPointPlace.class)) {
      redirect = new SourceImportPlace();
    }

    GWT.log("Redirecting to: " + redirect);

    return redirect;
  }
}
