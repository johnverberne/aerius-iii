package nl.overheid.aerius.wui.application.ui.air.sources.list;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;

import nl.overheid.aerius.wui.application.components.dummy.SimpleDummyComponent;
import nl.overheid.aerius.wui.application.components.views.DefaultTitleView;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    DefaultTitleView.class,
    SimpleDummyComponent.class
})
public class AirQualityListView extends BasicVueView {
  @Prop AirQualityListActivity presenter;
}
