/*
 * Copyright Dutch Ministry of Agriculture, Nature and Food Quality
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.application.activity;

import java.util.LinkedHashMap;
import java.util.Map;

import nl.overheid.aerius.wui.application.place.ApplicationTokenizers;
import nl.overheid.aerius.wui.application.place.air.AirQualityTokenizers;
import nl.overheid.aerius.wui.application.place.deposition.DepositionTokenizers;
import nl.overheid.aerius.wui.history.PlaceHistoryMapper;
import nl.overheid.aerius.wui.place.PlaceTokenizer;
import nl.overheid.aerius.wui.place.TokenizedPlace;

public class ApplicationPlaceHistoryMapper implements PlaceHistoryMapper {
  private final Map<String, PlaceTokenizer<? extends TokenizedPlace>> tokenizers = new LinkedHashMap<>();

  public ApplicationPlaceHistoryMapper() {
    init(ApplicationTokenizers.OVERVIEW);

    // Air quality
    init(AirQualityTokenizers.SOURCE);
    init(AirQualityTokenizers.SOURCE_IMPORT);
    init(AirQualityTokenizers.SOURCE_CREATE);
    init(AirQualityTokenizers.SOURCE_LIST);

    init(AirQualityTokenizers.CALCULATION_POINT);
    init(AirQualityTokenizers.MEASURE);
    init(AirQualityTokenizers.PREFERENCE);
    init(AirQualityTokenizers.RESULT);
    init(AirQualityTokenizers.LAYER);

    // Deposition
    init(DepositionTokenizers.SOURCE);
    init(DepositionTokenizers.SOURCE_IMPORT);
    init(DepositionTokenizers.SOURCE_CREATE);
    init(DepositionTokenizers.SOURCE_LIST);

  }

  private void init(final PlaceTokenizer<?> tokenizer) {
    tokenizers.put(tokenizer.getPrefix(), tokenizer);
  }

  @Override
  public String getToken(final TokenizedPlace value) {
    return value.getToken();
  }

  @Override
  public TokenizedPlace getPlace(final String fullToken) {
    final String trimmedToken = trimToken(fullToken);

    return tokenizers.keySet().stream()
        .filter(v -> trimmedToken.equals(v))
        .findFirst()
        .map(v -> {
          final String token = fullToken.substring(v.length());
          return tokenizers.get(v).getPlace(token);
        }).orElse(null);
  }

  private String trimToken(final String fullToken) {
    return trimEnd(trimStart(fullToken));
  }

  private String trimEnd(final String token) {
    return token.lastIndexOf("/") == token.length() - 1 ? trimEnd(token.substring(0, token.length() - 1)) : token;
  }

  private String trimStart(final String token) {
    return token.indexOf("/") == 0 ? trimStart(token.substring(1)) : token;
  }
}
