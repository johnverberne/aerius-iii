package nl.overheid.aerius.wui.application.components.dummy;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

@Component(name = "dummy-2")
public class SimpleSecondaryDummyComponent implements IsVueComponent {
  @Data String message = "dummy-bound (2)";

  @Data Integer count = 0;
}
