package nl.overheid.aerius.wui.application.components.importer;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import jsinterop.annotations.JsMethod;

@Component(name = "aer-import")
public class ApplicationImportComponent implements IsVueComponent {
  @JsMethod
  public void onImportClick() {

  }
}
