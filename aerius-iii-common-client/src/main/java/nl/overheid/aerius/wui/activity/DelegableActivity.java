package nl.overheid.aerius.wui.activity;

import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.place.Place;

public interface DelegableActivity<P extends Place> {
  default boolean delegate(final EventBus eventBus, final P place) {
    return false;
  }

  default boolean isDelegable(final Place place) {
    return false;
  }
}
