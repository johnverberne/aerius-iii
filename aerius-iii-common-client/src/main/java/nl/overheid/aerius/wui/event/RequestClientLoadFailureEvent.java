package nl.overheid.aerius.wui.event;

import nl.overheid.aerius.wui.event.SimpleGenericEvent;

public class RequestClientLoadFailureEvent extends SimpleGenericEvent<Boolean> {
  public RequestClientLoadFailureEvent(final boolean value) {
    super(value);
  }
}
