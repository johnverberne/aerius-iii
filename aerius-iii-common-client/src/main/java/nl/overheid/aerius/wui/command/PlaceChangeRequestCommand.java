package nl.overheid.aerius.wui.command;

import nl.overheid.aerius.wui.event.SimpleGenericEvent;
import nl.overheid.aerius.wui.place.ApplicationPlace;

public class PlaceChangeRequestCommand extends SimpleGenericEvent<ApplicationPlace> {
  public PlaceChangeRequestCommand(final ApplicationPlace obj) {
    super(obj);
  }
}
