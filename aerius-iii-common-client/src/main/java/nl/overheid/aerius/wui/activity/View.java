package nl.overheid.aerius.wui.activity;

public interface View<P> extends HasPresenter<P> {}
